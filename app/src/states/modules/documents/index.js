import { createSlice } from "@reduxjs/toolkit";
import { addDocumentSelector, markCompleted } from "./selectors";

const initialState = {
  newDocument: {
    fileHash: "",
  },
  loading: true,
  items: [],
};

const { actions, reducer } = createSlice({
  name: "session",
  initialState,
  reducers: {
    saveFileHash: (state, { payload }) => {
      const { fileHash } = payload;
      const { fileName } = payload;
      return { ...state, newDocument: { fileHash, fileName } };
    },
    createDocument: (state) => {
      return state;
    },
    createSmartContract: (state, { payload }) => {
      return state;
    },
    addDocument: (state, { payload }) => {
      const { items } = state;
      const { fileHash } = payload;
      const { fileName } = payload;
      const { status } = payload;
      const newDocument = { fileHash: "" };
      const newItems = addDocumentSelector(items, fileName, fileHash, status);
      return { ...state, items:newItems, newDocument };
    },
    addDocumentSigningStatus: (state, { payload }) => {
      const { items } = state;
      const { fileHash } = payload;
      const { status } = payload;
      const { fileName } = payload;
      const newDocument = { fileHash: "" };
      const newItems = markCompleted(items, fileName, fileHash, status);
      return { ...state, items:newItems, newDocument };
    },
    signDocument: (state) => {
      return state;
    },
    loadDocuments: (state) => {
      return state;
    },
    clearDocuments: (state) => {
      return {...state, items:[]}
    },
    loadDocumentsSuccess: (state, { payload }) => {
      const { items } = payload;
      return { ...state, loading: false, items };
    },
    loadDocumentsFail: (state, { payload }) => {
      return { ...state, loading: false, items: [] };
    },
  },
});

export const {
  saveFileHash,
  createDocument,
  createSmartContract,
  addDocument,
  signDocument,
  loadDocuments,
  loadDocumentsSuccess,
  loadDocumentsFail,
  clearDocuments,
  addDocumentSigningStatus,
} = actions;

export { default as documentSaga } from "./sagas";

export default reducer;

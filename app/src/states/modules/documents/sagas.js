import { takeLatest, select, takeEvery, put, fork } from "redux-saga/effects";
import {
  createSmartContract,
  createDocument,
  addDocument,
  signDocument,
  loadDocuments,
  loadDocumentsSuccess,
  LoadDocuemntsFail,
  addDocumentSigningStatus,
} from ".";
import { selectSigners } from "./selectors";
import status from '~/constants';

import { message } from "antd";

import Web3 from "web3";
import { abi as factoryAbi } from "~/contracts/Newtary.json";
import { abi as documentAbi } from "~/contracts/Document.json";

export default function* rootSaga() {
  yield takeLatest(createSmartContract, createContractSaga);
  yield takeEvery(createDocument, createDocumentSaga);
  yield takeLatest(signDocument, signDocumentSaga);
  yield takeLatest(loadDocuments, loadDocumentsSaga);
}

function* createDocumentSaga({ payload }) {
  const { history } = payload;
  const signers = selectSigners(payload);
  const fileHash = yield select(
    (state) => state.documents.newDocument.fileHash
  );
  const fileName = yield select(
    (state) => state.documents.newDocument.fileName
  );

  yield put(createSmartContract({ fileHash, fileName, signers }));
  yield put(addDocument({ fileHash, fileName, status: status.OWNED }));

  history.push("/documents");
}

function* createContractSaga({ payload }) {
  const { fileHash } = payload;
  const { signers } = payload;
  const { fileName } = payload
  const userAccount = yield select((state) => state.session.account);
  const documentFactoryContractAddress = process.env.REACT_APP_FACTORY_CONTRACT;

  const creatingContractMessage = message.loading(
    `Creating smart contact for document ${fileHash}`,
    0
  );
  const web3 = new Web3(window.ethereum);
  const factoryContract = yield new web3.eth.Contract(
    factoryAbi,
    documentFactoryContractAddress
  );
  try {
    yield factoryContract.methods.newDocument(fileName, fileHash, signers).send({
      gas: 2000000,
      from: userAccount,
    });
    creatingContractMessage();
    message.success(
      `Smart contract for document ${fileHash} succefully created.`,
      5
    );
    yield put(loadDocuments());
  } catch (e) {
    console.log(e);
    creatingContractMessage();
    message.error(`Creating contract for file hash ${fileHash} failed`, 5);
  }
}

function * signDocumentSaga({ payload }) {
  const { fileHash } = payload;
  var fileName;
  var documentContractAddress;
  const userAccount = yield select((state) => state.session.account);
  const documentFactoryContractAddress = process.env.REACT_APP_FACTORY_CONTRACT;

  const web3 = new Web3(window.ethereum);
  const factoryContract = yield new web3.eth.Contract(
    factoryAbi,
    documentFactoryContractAddress
  );
  var documentContractAddress;
  try {
    const documentData = yield factoryContract.methods
      .getDocumentData(fileHash)
      .call({
        from: userAccount,
      });
    documentContractAddress = documentData.contractAddress;
    fileName = documentData.name;
  } catch (e) {
    console.log(e);
    return;
  }
  const removeSigningDocumentMessage = message.loading(
    `Signing document ${fileHash}`,
    0
  );
  yield put(addDocument({fileHash, fileName, status: status.SIGNED}))
  const documentContract = yield new web3.eth.Contract(
    documentAbi,
    documentContractAddress
  );
  try {
    yield documentContract.methods.sign(fileHash).send({
      gas: 2000000,
      from: userAccount,
    });
    removeSigningDocumentMessage();
    message.success(
      `Signing process for ${fileHash} completed succefully :)`,
      5
    );
  } catch (e) {
    console.log(e);
    removeSigningDocumentMessage();
    message.error(`Signing for file hash ${fileHash} failed :(`, 5);
  }
  yield put(loadDocuments())
}

function* loadDocumentsSaga({ payload }) {
  const userAccount = yield select((state) => state.session.account);
  const documentFactoryContractAddress = process.env.REACT_APP_FACTORY_CONTRACT;
  const web3 = new Web3(window.ethereum);
  const factoryContract = yield new web3.eth.Contract(
    factoryAbi,
    documentFactoryContractAddress
  );
  yield loadAccountRelatedDocuments(factoryContract, userAccount);
  yield loadCompletedDocuments(factoryContract, userAccount);
}

function* loadCompletedDocuments(factory, account) {
  const documents = yield select((state) => state.documents.items);
  for( var i = 0; i < documents.length; i++) {
    const documentData = yield factory.methods.getDocumentData(documents[i].hash).call({from: account});
    const documentContractAddress = documentData.contractAddress;
    const fileName = documentData.name;
    yield fork(loadDocumentSigningStatus, documents[i].hash, fileName, account, documentContractAddress);
  };
}

function* loadDocumentSigningStatus(fileHash, fileName, account, documentContractAddress) {
  const web3 = new Web3(window.ethereum);
  const documentContract = yield new web3.eth.Contract(
    documentAbi,
    documentContractAddress
  );
  try {
    const status = yield documentContract.methods.isCompleted().call({from: account});
    yield put(addDocumentSigningStatus({fileHash, fileName, status}))
  }
  catch(e) {
    console.log( 'Document contract still being created' )
  }
}

function* loadAccountRelatedDocuments(factory, account) {
  var documentsCount;
  try {
    documentsCount = yield factory.methods.accountTotalFiles().call({
      from: account,
    });
  } catch (e) {
    console.log(e);
  }
  for (var i = 0; i < documentsCount.owned; i++) {
    yield fork(loadOwned, i, factory, account);
  }

  for (i = 0; i < documentsCount.signed; i++) {
    yield fork(loadSigned, i, factory, account);
  }

  for (i = 0; i < documentsCount.requested; i++) {
    yield fork(loadRequested, i, factory, account);
  }
}

function* loadOwned(index, contract, account) {
  try {
    const fileHash = yield contract.methods
      .accountOwnedDocuments(index)
      .call({ from: account });
    const documentData = yield contract.methods
      .getDocumentData(fileHash)
      .call({ from: account });
    const fileName = documentData.name;
    yield put(addDocument({fileHash, fileName, status: status.OWNED}))
  } catch (e) {
    console.log(e);
  }
}

function* loadRequested(index, contract, account) {
  try {
    const fileHash = yield contract.methods
      .accountRequestedDocuments(index)
      .call({ from: account });
      const documentData = yield contract.methods
      .getDocumentData(fileHash)
      .call({ from: account });
    const fileName = documentData.name;
    yield put(addDocument({fileHash, fileName, status: status.SIGNATURE_REQUESTED}))
  } catch (e) {
    console.log(e);
  }
}

function* loadSigned(index, contract, account) {
  try {
    const fileHash = yield contract.methods
      .accountSignedDocuments(index)
      .call({ from: account });
    const documentData = yield contract.methods
      .getDocumentData(fileHash)
      .call({ from: account });
    const fileName = documentData.name;
    yield put(addDocument({fileHash, fileName, status: status.SIGNED}))
  } catch (e) {
    console.log(e);
  }
}

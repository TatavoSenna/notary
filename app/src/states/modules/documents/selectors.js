import status from "~/constants";

export function selectSigners(payload) {
  return payload.values.users.map((signer) => signer.account_id);
}

function compare( a, b ) {
  if ( a.name < b.name ){
    return -1;
  }
  if ( a.name > b.name ){
    return 1;
  }
  return 0;
}

export function addDocumentSelector(
  documentsList,
  newFileName,
  newDocumentHash = null,
  newStatus
) {
  var newDocumentList = [];
  var newDocumentData;

  if (newDocumentHash) {
    newDocumentData = {
      name: newFileName,
      hash: newDocumentHash,
      owned: newStatus === status.OWNED,
      requested: newStatus === status.SIGNATURE_REQUESTED,
      signed: newStatus === status.SIGNED,
    };
    newDocumentList = [newDocumentData];
  }

  documentsList.forEach((element) => {
    if (element.hash === newDocumentHash) {
      newDocumentData.owned = element.owned || newDocumentData.owned;
      if (!newDocumentData.owned) {
        newDocumentData.requested =
          element.requested && newDocumentData.requested;
        newDocumentData.signed = element.signed && newDocumentData.signed;
      }
    } else {
      newDocumentList.push(element);
    }
  });

  return newDocumentList.sort(compare);
}

export function markCompleted(documentsList, fileName, hash, status) {
  var newDocumentsList = [];
  documentsList.forEach((item) => {
    const newItem = { ...item };
    if (hash === item.hash) {
      newItem.isCompleted = status;
    }
    newDocumentsList.push(newItem);
  });
  return newDocumentsList.sort(compare);
}

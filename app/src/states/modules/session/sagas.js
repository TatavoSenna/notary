import { put, takeLatest } from "redux-saga/effects";
import detectEthereumProvider from "@metamask/detect-provider";

import {
  getAccount,
  setAccount,
  initializeMetamask,
  setConnectionStatus,
} from ".";
import { loadDocuments, clearDocuments } from "~/states/modules/documents";

export default function* rootSaga() {
  yield takeLatest(getAccount, getAccountSaga);
  yield takeLatest(initializeMetamask, initializeMetamaskSaga);
}

function* getAccountSaga({ payload }) {
  const provider = yield detectEthereumProvider();
  var accounts = [];
  if (provider) {
    if (provider === window.ethereum) {
      if (window.ethereum.chainI != 3) {
        console.log(
          "The app is currently available only in the Ropsten network"
        );
      }
      try {
        accounts = yield window.ethereum.request({
          method: "eth_requestAccounts",
        });
      } catch {
        console.log("user rejected the connection");
        return
      }
      const account = accounts[0];
      yield put(setAccount({ account }));

      const { history } = payload;
      history.push("/documents");
    } else {
      console.log(
        "Connection to Metamask failed. Do you have multiple wallets insctalled?"
      );
    }
  } else {
    console.log("Metamask not detected!. Please install MetaMask!");
  }
}

function* initializeMetamaskSaga({ payload }) {
  const { dispatch } = payload;
  const { history } = payload;
  const provider = yield detectEthereumProvider();
  if (!provider) {
    yield put(
      setConnectionStatus({
        connectionButtonActive: false,
        connectionMessage:
          "No ethereum provider found.\n Please install metamask",
      })
    );
    return;
  }
  if (provider !== window.ethereum) {
    yield put(
      setConnectionStatus({
        connectionButtonActive: false,
        connectionMessage:
          "Connection to metamask failed.\n Do you have multiple wallets installed?",
      })
    );
    return;
  }
  console.log(window.ethereum.chainId)
  if (window.ethereum.chainId != parseInt(process.env.REACT_APP_NETWORK_ID)) {
    yield put(
      setConnectionStatus({
        connectionButtonActive: false,
        connectionMessage:
          "Newtary only works on the Ropsten Network. \n Please Change the network on Metamask.",
      })
    );
    history.push("/");
    return;
  }

  yield put(
    setConnectionStatus({
      connectionButtonActive: true,
      connectionMessage:
        "",
    })
  );

  window.ethereum.on("chainChanged", (_chainId) => {
    console.log(process.env.REACT_APP_NETWORK_ID)
    if (_chainId != parseInt(process.env.REACT_APP_NETWORK_ID)) {
      dispatch(
        setConnectionStatus({
          connectionButtonActive: false,
          connectionMessage:
            "Newtary only works on the Ropsten Network. \n Please Change the network on Metamask.",
        })
      );
      history.push("/");
    } else {
      dispatch(
        setConnectionStatus({
          connectionButtonActive: true,
          connectionMessage:
            "",
        })
      );
    }
  });

  window.ethereum.on("accountsChanged", (accounts) => {
    if (accounts.length === 0) {
      history.push("/");
    }
    const account = accounts[0];
    dispatch(setAccount({ account }));
    dispatch(clearDocuments());
    dispatch(loadDocuments());
  });
}

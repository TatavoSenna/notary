import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  account: null,
  connectionButtonActive: true,
  connectionMessage: ""
};

const { actions, reducer } = createSlice({
  name: "session",
  initialState,
  reducers: {
    getAccount: (state) => {
        return {...state}
    },
    setAccount: (state, { payload }) => {
      const { account } = payload
      return { ...state, account }
    },
    startMetamaskAccountChangeListener(state, { payload }) {
      return {...state}
    },
    initializeMetamask: (state, { payload }) => {
      return {...state}
    },
    setConnectionStatus: (state, { payload }) => {
      const { connectionButtonActive } = payload;
      const { connectionMessage } = payload;
      return{...state, connectionButtonActive, connectionMessage}
    }
  }
})

export const { getAccount, setAccount, initializeMetamask, setConnectionStatus } = actions;

export { default as sessionSaga } from "./sagas";

export default reducer;

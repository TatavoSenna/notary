import storage from 'redux-persist/lib/storage'
import { persistReducer } from 'redux-persist'

const persist = (reducers) => {
	const persistedReducer = persistReducer(
		{
			key: 'newtary',
			storage,
			whitelist: ['session'],
		},
		reducers
	)

	return persistedReducer
}

export default persist
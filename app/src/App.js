import "antd/dist/antd.css";
import "./App.css";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { store, persistor } from "./states/store";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Home from "./pages/home";
import NewDocument from "./pages/new_document";
import Signers from "./pages/signers";
import Documents from "./pages/documents";
import Metamask from "~/components/metamask";

function App() {
  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <Router>
          <Metamask>
            <Switch>
              <Route path="/documents" component={Documents} />
              <Route path="/signers" component={Signers} />
              <Route path="/new-document" component={NewDocument} />
              <Route path="/" component={Home} />
            </Switch>
          </Metamask>
        </Router>
      </PersistGate>
    </Provider>
  );
}

export default App;

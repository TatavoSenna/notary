import { useDispatch } from "react-redux";
import { useEffect } from "react";
import { initializeMetamask } from "~/states/modules/session";
import { useHistory } from 'react-router-dom';

function Metamask({ children }) {
  const dispatch = useDispatch();
  const history = useHistory();

  useEffect(() => {
    dispatch(initializeMetamask({dispatch, history}))
  });

  return <div>{children}</div>;
}

export default Metamask;

import "antd/dist/antd.css";
import { Typography, Layout, Row, Col, Avatar } from "antd";
import { UserOutlined } from '@ant-design/icons';
import { useSelector } from "react-redux";

const { Header } = Layout;
const { Title } = Typography;

function NHeader() {
  const account = useSelector((state) => state.session.account);

  return (
    <Header>
      <Layout style={{ backgroundColor: "rgba(0,0,0,0)" }}>
        <Row>
          <Col span={18}>
            <Title style={{ color: "white" }}>Newtary</Title>
          </Col>
          <Col span={6}>
              <Avatar icon={<UserOutlined />}/>
            <span style={{color: "white", marginLeft: "10px"}}>{account}</span>
          </Col>
        </Row>
      </Layout>
    </Header>
  );
}

export default NHeader;

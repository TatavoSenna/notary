const status = {
    OWNED: "owned",
    SIGNATURE_REQUESTED: "requested",
    SIGNED: "signed"
}

export default status;
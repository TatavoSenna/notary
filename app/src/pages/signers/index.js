import "antd/dist/antd.css";
import "./signers.css";

import { useDispatch } from "react-redux";
import { createDocument } from "~/states/modules/documents";

import { useHistory } from "react-router-dom";

import { Form, Input, Button, Space, Layout, Typography } from "antd";
import { MinusCircleOutlined, PlusOutlined } from "@ant-design/icons";

const { Header, Content } = Layout;
const { Title } = Typography

function Signers() {

const dispatch = useDispatch();
const history = useHistory();

  const onFinish = (values) => {
    dispatch(createDocument({values, history}));
  };

  return (
    <Layout className="full-height">
      <Header></Header>
      <Content>
        <div className="signers-content-wrapper">
          <div className="signers-box">
            <Title style={{marginBottom: "40px"}} level={3}>Add the account ids that must sign the document</Title>
            <Form
              name="dynamic_form_nest_item"
              onFinish={onFinish}
              autoComplete="off"
            >
              <Form.List name="users">
                {(fields, { add, remove }) => (
                  <>
                    {fields.map((field) => (
                      <Space
                        key={field.key}
                        style={{ display: "flex", marginBottom: 8 }}
                        align="baseline"
                      >
                        <Form.Item
                          {...field}
                          name={[field.name, "account_id"]}
                          fieldKey={[field.fieldKey, "account"]}
                          rules={[
                            { required: true, message: "Account id is required" },
                          ]}
                          style={{width: "400px"}}
                        >
                          <Input placeholder="Signer Account Id" />
                        </Form.Item>
                        {/* <MinusCircleOutlined
                          onClick={() => { 
                            console.log(fields)
                            remove(field.name)}}
                        /> */}
                      </Space>
                    ))}
                    <Form.Item>
                      <Button
                        type="dashed"
                        onClick={() => add()}
                        block
                        icon={<PlusOutlined />}
                      >
                        Add Signer
                      </Button>
                    </Form.Item>
                  </>
                )}
              </Form.List>
              <Form.Item>
                <Button type="primary" htmlType="submit">
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </Content>
    </Layout>
  );
}

export default Signers;

import "antd/dist/antd.css";
import "./NewDocument.css";
import { useDispatch } from "react-redux";
import { Layout, Upload } from "antd";
import { InboxOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";
import { saveFileHash } from "~/states/modules/documents";

const { Header, Content } = Layout;
const { Dragger } = Upload;

function NewDocument() {
  const history = useHistory();
  const dispatch = useDispatch();

  const fileChosen = (uploadInfo) => {
    const ipfsClient = require("ipfs-http-client");
    const ipfs = ipfsClient({
      host: "ipfs.infura.io",
      port: 5001,
      protocol: "https",
    });
    const reader = new window.FileReader();
    reader.readAsArrayBuffer(uploadInfo.file);
    reader.onloadend = async () => {
      const result = await ipfs.add(Buffer(reader.result));
      dispatch(saveFileHash({fileHash: result.cid.string, fileName: uploadInfo.file.name, history}));
      };
      history.push('/signers');
  };

  const props = {
    name: "file",
    multiple: false,
    directory: false,
    customRequest: fileChosen,
  };

  return (
    <Layout className="full-height">
      <Header></Header>
      <Content>
        <div className="upload-content-wrapper">
          <div className="upload-box">
            <Dragger {...props}>
              <p className="ant-upload-drag-icon">
                <InboxOutlined />
              </p>
              <p className="ant-upload-text">
                Click or drag the document file to this area to upload
              </p>
              <p className="ant-upload-hint">
                Please upload 1 doument at a time
              </p>
            </Dragger>
          </div>
        </div>
      </Content>
    </Layout>
  );
}

export default NewDocument;

import "antd/dist/antd.css";
import { useDispatch, useSelector } from "react-redux";
import { Layout, Row, Col, Button } from "antd";
import { useHistory } from "react-router-dom";
import { Typography } from "antd";
import { getAccount } from "~/states/modules/session";

const { Title, Text } = Typography;

function Home() {
  const dispatch = useDispatch();
  const history = useHistory();

  const { connectionButtonActive, connectionMessage } = useSelector(
    (state) => state.session
  );

  const handleGetAccount = (event) => {
    dispatch(getAccount({ history }));
  };

  return (
    <Layout className="full-height">
      <Row className="full-height">
        <Col span={12} className="full-height">
          <div className="home_column">
            <Title className="home_content">
              <Text strong>NEWTARY</Text>
            </Title>
            <Button
              disabled={!connectionButtonActive}
              type="primary"
              className="home_content"
              onClick={handleGetAccount}
            >
              connect with Metamask
            </Button>
            <Text style={{alignSelf: "center", marginTop: "10px"}}>{connectionMessage}</Text>
          </div>
        </Col>
        <Col
          span={12}
          className="home_column full-height home_illustration"
        ></Col>
      </Row>
    </Layout>
  );
}

export default Home;

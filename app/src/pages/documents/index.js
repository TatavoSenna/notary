import "antd/dist/antd.css";
import "./documents.css";

import { useEffect } from 'react';
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";

import { Layout, Typography, List, Button} from "antd";
import NHeader from '~/components/header';
import { EyeOutlined, FileDoneOutlined, SolutionOutlined, FileTextOutlined, CheckCircleFilled } from "@ant-design/icons";

import { signDocument, loadDocuments } from '~/states/modules/documents'
import { IconMap } from "antd/lib/result";

const { Content } = Layout;
const { Title } = Typography;

function Documents() {
  const dispatch = useDispatch();
  const history = useHistory();
  const documents = useSelector((state) => state.documents.items);

  const handleNewDocument = (event) => {
    history.push("/new-document");
  };

  const handleSign = (fileHash) => {
    dispatch(signDocument({fileHash}));
  }

  const handleView = (fileHash) => {
    window.open(`https://gateway.ipfs.io/ipfs/${fileHash}`, "_blank");
  }

  const getActions = (item) => {
    const actions = [<EyeOutlined style={{fontSize: '24px', color: '#08c'}} onClick={() => handleView(item.hash)}/>];
      if (item.requested) {
        actions.push(<FileDoneOutlined style={{fontSize: '24px', color: '#08c'}} onClick={() => handleSign(item.hash)}/>);
      }
      if (item.signed) {
        actions.push(<FileDoneOutlined style={{fontSize: '24px', color: 'Green'}}/>);
      }
      if (item.isCompleted) {
        actions.push(<CheckCircleFilled style={{fontSize: '24px', color: 'Green'}}/>);
      }
    return actions
  }

  useEffect(() => dispatch(loadDocuments()), [0]);

  return (
    <Layout className="full-height">
      <NHeader></NHeader>
      <Content>
        <div className="documents-content-wrapper">
          <div className="documents-box">
            <Title level={3}>Contracts</Title>
            <List
              bordered
              header={
                <div style={{textAlign: "right", borderBottom: "1px solid LightGrey"}}>
                  <Button type="primary" style={{marginBottom: "20px"}} onClick={handleNewDocument}>
                    New Document
                  </Button>
                </div>
              }
              dataSource={documents}
              renderItem={(item) => (
                <List.Item
                  actions={getActions(item)}
                  style={{borderBottom: "1px solid LightGrey"}}
                >
                  { item.owned ? <SolutionOutlined style={{ fontSize: '24px'}}/> : <FileTextOutlined style={{ fontSize: '24px'}}/>}
                  <Typography.Text style={{width: "600px"}}>{item.name}</Typography.Text>
                </List.Item>
              )}
            />
          </div>
        </div>
      </Content>
    </Layout>
  );
}

export default Documents;

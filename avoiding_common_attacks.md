# Common attacks
Newtary doesn't handle any money and I couldn't find any possible common attack that could cause damage to the system.
The only thing I could see was that, because It has to loop over the requested signatures on the database to remove a signer it could cause an out of gas problem. It would be only to the caller's prejudice, but I limited the number of signers anyway.

I also made it pausable, so that the owner can pause it in case of bugs.

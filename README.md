# Newtary

Ethereum based notary system

## Motivation
Lately I've bee working on many projects related to digital document signing, and using a bunch of Saas platforms that deliver this kind of service, such as Docusign and HelloSign. Even the official Brasilan social security number, CPF, now has a digital signing mechanism, and it is private key based. But of course it is centralized in the hand of some private company.   
I don't like brasilian Notaries. They are the icon of the centuries old brazilian burocracy. They make everything difficult, just to make tons of money for those priviledged enough to have a license.
After learning more about the Ethereum blockchain and IPFS I decided to build a Notary, called Newtary, that will do exactly what a brazilian notary does. register documents and autenticate the people that signed it.
The IPFS content addressing and Ethereum smart contracts are the perfect technology to replace this old, manual, paper based centralized system.

## General Architecture
I put together a prototype app that lets the user upload a document file to IPFS and enter a list of ethereum account numbers that he wants to sign the document.
The app then sends these data to the Newtary smart contract. The Newtary contract deploys a Document contract that represents the document on the blockchain and register the signatures.    
The Newtary contract also mantains a database of all file Hashes and their associated contracts, as well as all the accounts that own or sign the contracts. The app uses these database to populate the app documents list.

## Project Structure
The project separated in 2 main folders. The /contracts is a truffle project with the solidity contracts. The /app folder is the frontend react app.
On React I used the Redux and Saga javascript libraries to organize the information flow through the app. The interaction with web3 and conracts happens on `/app/src/states/modules/documents/sagas.js` and the interaction with Metamask happens on `/app/src/modules/session/sagas.js`.

## Running the app locally
1. Move to the app folder with `cd ../app`
2. Install the app dependencies with `yarn`
3. Run the React app with `yarn start`
4. Access the app on localhost:3000
5. Make sure Metamask is connected to the Ropsten Network and you are ready to take Newtary for a spin.
6. You can also checkout the app deployed on https://gateway.ipfs.io/ipfs/Qmc1E4uFSBZ7wDi5oQJbmJH9viyowHPtmJcKBFeBkPUmn4/

## Running the tests
1. Make sure you are on the /contracts folder
2. install the node dependencies with `yarn`
3. install ganache command line blockchain with `yarn global add ganache-cli`
4. run ganache with `ganache-cli`
3. execute `truffle test`



# Design Patterns

I used 3 design Patterns:

- Factory    
Newtary contract is a factory, as it creates all document signing contracts. It also keeps track of every contract it creates.

- Ownable    
Newtary contract has an owner, and it is the only one that can pause the contract

- Pausable    
Newtary is Pausable, and only the owner can pause it.

# Other design considerations
I was greatly in doubt of how to store all the Document and users data needed to have a nice user interface. My first idea was to have a big json with all the data and store it on IPFS. I even spinned up a IPFS node on AWS to do that. But then I realized everyone wouldnt be able to protect this backend data using ethereum accounts.
So I decided to put the data on the Newtary contract iself. But I wanted to display lists of files on the app, and contracts returning arrays seems to be still experimental on Solidity. So I devised a system that give me the number of contracts an account is involved. With the number of contracts the app pulls contracts by status, and one by one. Not sure this is a nice pattern. But for my surprise it worked pretty fast, so I addopted it.
After writing all the code I realized the contracts could be even more modularized if I had build a separate contract only for the "Database". But I didn't have the time to do it.

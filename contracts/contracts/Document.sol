// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.4;

import './Newtary.sol';


/**
* @title Represents a document to be signed by requested accounts
* @author Luiz Senna
* @dev It stores the file CID, accounts from which signatures where requested and the newtary of the document
* Upon Signing he also calls the Newtary contract so that it can mantain track of each account status on every document it is involved.
*/
contract Document {
    
    event NewDocument (
        string indexed eventType,
        string fileHash
    );

    event SignedDocument (
        string indexed eventType,
        string fileHash
    );
    
    /**
    * @dev USed to control which account were requested to sign and which already signed.
    */
    enum SigningStatus { Unknown, Requested, Signed }

    /**
    * @dev the Newtary contract
     */
    address private newtary;

    /**
    * @dev The document file CID 
    */
    string public ipfsHash;

    /**
    * @dev Keeps track of accounts that signed and that needs to sign the document
    */
    mapping(address => SigningStatus) public signers;
    uint requestedCount;
    uint signedCount = 0;
    
    /**
    * @dev Only requested accounts can sign
    */
    modifier onlyRequested (address sender) {
        require (signers[sender] == SigningStatus.Requested);
        _;
    }

    /**
    * @dev The account must send the correct file CID to ensure he knows what he is signing
    */
    modifier sameDocument(string memory documentHash) {
        require (keccak256(abi.encodePacked(ipfsHash)) == keccak256(abi.encodePacked(documentHash)));
        _;
    }

    /**
    * @dev In some situations we need to loop through the requested signatures, so we limit the number of signers.
     */
    modifier reasonableSignersQuantity(address[] memory _signers){
        require(_signers.length < 100);
        _;
    }
    
    /**
    * @dev Creates the contract and stores file CID and signers 
    */
    constructor (string memory ipfs_document_hash, address[] memory _signers) reasonableSignersQuantity(_signers) {
        newtary = msg.sender;
        ipfsHash = ipfs_document_hash;
        requestedCount = _signers.length;
        for (uint i=0; i < _signers.length; i++) {
            signers[_signers[i]] = SigningStatus.Requested;
        }
        emit NewDocument('DocumentRegistered', ipfs_document_hash);
    }

    /**
    * @dev Registers the signature of caller of document. It calls the Newtary contract to keep its database updated.
    */
    function sign(string memory documentHash)
        public
        sameDocument(documentHash)
        onlyRequested(msg.sender) {
        signers[msg.sender] = SigningStatus.Signed;
        signedCount++;
        Newtary newtaryContract = Newtary(newtary);
        newtaryContract.registerSignature(msg.sender);
        emit SignedDocument('DocumentSigned', documentHash);
    }

    /**
    * @dev check if user has already signed
    */
    function hasSigned(address signer) public view returns (bool signed) {
        signed = signers[signer] == SigningStatus.Signed;
    }

    /**
    * @dev check if an account has signed the document
    */
    function isSigner() public view returns (bool isSignerResult) {
        isSignerResult = signers[msg.sender] == SigningStatus.Requested || signers[msg.sender] == SigningStatus.Signed;
    }

    function isCompleted() public view returns(bool _isCompleted) {
        _isCompleted = requestedCount == signedCount;
    }

    fallback () external {
        revert();
    }
}
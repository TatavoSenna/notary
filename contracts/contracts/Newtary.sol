// SPDX-License-Identifier: GPL-3.0
pragma solidity ^0.7.4;
import './Document.sol';
import './Ownable.sol';

/**
* @title A notary system for registering documents and signatures on the Ethereum blockchain
* @author Luiz Senna
* @dev This contract acts both as a factory for the document's contracts and a database for the signer's accounts.
* Accounts should call newDocument sending the file's ipfs CID and an array of addresses that need to sign the document.
* Newtary keeps a mapping from the file ipfs CID to the contract address on the documents public variable. It also
* keeps a mapping for every account involved in any contract in the usersData private variable.
* To sign the document the signer must call the Document contract directly. The Document does call the Newtary contract to
* update its database.
*/
contract Newtary is Ownable{

    struct UserData {
        string[] owned;
        string[] requested;
        string[] signed;
    }

    struct DocumentData {
        address contractAddress;
        string name;
    }

    /**
    * @dev The documents variable keeps a mapping between the files ipfs CIDs and their contract addreses.
    */
    mapping (string => DocumentData) private documentsData;

    /**
    * @dev The userData variable keeps a mapping between an account and the files it intereacts with.
    */
    mapping (address => UserData) private usersData;

    /**
    * @dev used to implement the pausable patern
     */
    bool public paused = false;

    modifier stopInEmergency { require(!paused); _; }
    
    /**
    * @dev Used to check if a file has not been already registered
    */
    modifier documentNotRegistered(string memory documentHash) {
        require(documentsData[documentHash].contractAddress == address(0)) ;
        _;
    }

    /**
    * @dev Checks if the caller is a contract deployed by newtary
    */
    modifier onlyRegisteredContract(address callerAddress) {
        Document documentContract = Document(callerAddress);
        string memory documentFileHash = documentContract.ipfsHash();
        require(documentsData[documentFileHash].contractAddress == callerAddress, 'Caller must be the original document contract');
        _;
    }

    /**
    * @dev Register a new document.
    */
    function newDocument(string memory _name, string memory fileHash, address[] memory _signers)
     public 
     documentNotRegistered(fileHash)
     stopInEmergency()
     {
        //  deploys the contract
        address newDocumentContract = address(new Document(fileHash, _signers));

        //saves data to the database
        documentsData[fileHash].contractAddress = newDocumentContract;
        documentsData[fileHash].name = _name;
        usersData[msg.sender].owned.push(fileHash);
        for(uint16 i = 0; i < _signers.length; i++ ) {
            usersData[_signers[i]].requested.push(fileHash);
        }
    }

    /**
    * @dev getDocumentHash(string memory hash)
    */
    function getDocumentData(string memory fileHash) public view returns (address contractAddress, string memory name) {
        contractAddress = documentsData[fileHash].contractAddress;
        name = documentsData[fileHash].name;
    }

    /**
    * @dev Checks if a document is registered.
    */
    function isRegistered(string memory fileHash) public view returns (bool isRegisteredResult) {
        isRegisteredResult = documentsData[fileHash].contractAddress != address(0);
    }

    /**
    * @dev pauses the contract
    */
     function pause() public onlyOwner() {
         paused = true;
     }


    /**
    * @dev pauses the contract
    */
     function unpause() public onlyOwner() {
         paused = false;
     }

    /**
    * @dev updates the user database when an accoount signs a document. It is called by the Document contract.
    */
    function registerSignature(address signer) public {
        
        // gets the file hash from the document contract
        Document documentContract = Document(msg.sender);
        string memory documentFileHash = documentContract.ipfsHash();

        // add the file Has to the account signed array
        usersData[signer].signed.push(documentFileHash);

        // look for the file in the account's requested array and deletes it
        uint requestIndex;
        for(uint i = 0; i < usersData[signer].requested.length; i++) {
            if (keccak256(abi.encodePacked(documentFileHash)) != keccak256(abi.encodePacked(usersData[signer].requested[i]))) {
                requestIndex = i;
                break;
            }
        }
        usersData[signer].requested[requestIndex] = usersData[signer].requested[usersData[signer].requested.length - 1];
        usersData[signer].requested.pop();
    }

    /**
    * @dev Return the number of documents an account is involved with.
    */
    function accountTotalFiles() public view returns (uint owned, uint requested, uint signed) {
        owned = usersData[msg.sender].owned.length;
        requested = usersData[msg.sender].requested.length;
        signed = usersData[msg.sender].signed.length;
    }

    /**
    * @dev gets a document owned by the account.
    */
    function accountOwnedDocuments(uint index) public view returns (string memory fileHash) {
         fileHash = usersData[msg.sender].owned[index];
    }

    /**
    * @dev gets a document where the account's signature has been requested.
    */
    function accountRequestedDocuments(uint index) public view returns (string memory fileHash) {
         fileHash = usersData[msg.sender].requested[index];
    }

    /**
    * @dev gets an document the account has signed.
    */
    function accountSignedDocuments(uint index) public view returns (string memory fileHash) {
         fileHash = usersData[msg.sender].signed[index];
    }

    fallback () external {
        revert();
    }

}




